$(document).ready(function () {
    $('.slick-container').slick({
        dots: false,
        infinite: true,
    });

    $('.comercial-section').slick({
        dots: false,
        infinite: true,
        centerMode: true,
        slidesToShow: 2,
        slidesToScroll: 2
    });

    var controller = new ScrollMagic.Controller();

    // create a scene
    new ScrollMagic.Scene({
        duration: 100,
        offset: 100
    })
        .setTween("#hero-text-container", { opacity: 0 })
        .addIndicators()
        .addTo(controller);

    new ScrollMagic.Scene({
        duration: 300,
        offset: 300
    })
        .setTween("#phone-slider-image", { marginLeft: '-100%' })
        .addIndicators()
        .addTo(controller);


    new ScrollMagic.Scene({
        duration: 200,
        offset: 200
    })
        .setTween("header", { opacity: 0, marginTop: '-100px' })
        .addIndicators()
        .addTo(controller);


    new ScrollMagic.Scene({
        offset: 100
    })
        .setTween(".sticky-part", { opacity: 0, overflow: 'hidden' })
        .addIndicators()
        .addTo(controller);


    $(window).scroll(function (e) {
        const scrollTop = e.target.scrollingElement.scrollTop;
        if (scrollTop > 450 && scrollTop < 1500) {
            $(".phone-container").css({ "margin-top": (scrollTop - 370) + "px" });
        } else {
            $(".phone-container").css({ "margin-top": "80px", "position": "relative" });
        }
    });
    $("#dialog-close-btn").click(function (e) {
        $("dialog").css("display", "none");
    });
});